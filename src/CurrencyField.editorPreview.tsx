import { Component, ReactNode, createElement } from "react";
import { CurrencyFieldPreviewProps } from "../typings/CurrencyFieldProps";

declare function require(name: string): string;

export class preview extends Component<CurrencyFieldPreviewProps> {
    render(): ReactNode {
        return <div>CurrencyField</div>;
    }
}

export function getPreviewCss(): string {
    return require("./ui/CurrencyField.css");
}
