import { Component, ReactNode, createElement } from "react";
import { CurrencyFieldContainerProps } from "../typings/CurrencyFieldProps";
import Big from "big.js";
import "./ui/CurrencyField.css";

export default class CurrencyField extends Component<CurrencyFieldContainerProps, {value: string}> {
    private readonly onUpdateHandle = this.onUpdate.bind(this);
    private readonly onLeaveHandle = this.onLeave.bind(this);

    constructor(props: CurrencyFieldContainerProps) {
        super(props);
        this.state = {value: this.props.textAttribute.value?.toString() || '0'}
    }

    render(): ReactNode {
        return (<div className="mx-textbox form-group col-sm-9">
                <div className="sign">&euro;</div>
                <input
                    type="text"
                    className={`form-control`}
                    value={this.state.value}
                    onChange={this.onUpdateHandle}
                    onBlur={this.onLeaveHandle}
                />
                </div>
                )
    }

    onUpdate(event: React.FormEvent<HTMLInputElement>) {
        console.info("onUpdate started");
        this.setState({value :  event.currentTarget.value});     
    }

    onLeave() {
        console.info("Leaving started");
        let newValue = new Big(this.state.value);
        this.props.textAttribute.setValue(newValue);
    }
}