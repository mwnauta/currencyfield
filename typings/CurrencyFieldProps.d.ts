/**
 * This file was generated from CurrencyField.xml
 * WARNING: All changes made to this file will be overwritten
 * @author Mendix UI Content Team
 */
import { CSSProperties } from "react";
import { EditableValue } from "mendix";
import { Big } from "big.js";

export interface CurrencyFieldContainerProps {
    name: string;
    class: string;
    style?: CSSProperties;
    tabIndex?: number;
    id: string;
    textAttribute: EditableValue<Big>;
}

export interface CurrencyFieldPreviewProps {
    class: string;
    style: string;
    textAttribute: string;
}
